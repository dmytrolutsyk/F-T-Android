package com.example.ft_android.FonctoinnalTest

import android.app.PendingIntent.getActivity
import android.service.autofill.Validators.not
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.TypeTextAction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.ft_android.MainActivity
import com.example.ft_android.R
import org.hamcrest.core.Is.`is`
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

   @Test
    fun succesfullyLaunchApp() {
        ActivityScenario.launch(MainActivity::class.java)
    }

    @Test
    fun loginInputDisplayed() {
        onView(withId(R.id.UsernameField)).check(matches(isEnabled()))
    }

    @Test
    fun fieldEnable() {
        val login = "dmytro"
        onView(withId(R.id.UsernameField)).perform(typeText(login), ViewActions.closeSoftKeyboard())
        onView(withId(R.id.PasswordField)).perform(TypeTextAction(login))
        onView(withId(R.id.UsernameField)).check(matches(withText(login)))
        onView(withId(R.id.PasswordField)).check(matches(withText(login)))
    }

    /*@Test
    fun toastError(){
        onView(withId(R.id.ValidateBtn)).perform(click())
        onView(withText(R.string.toast_error)).check(matches(isDisplayed()))
    }
*/
    @Test
    fun testNavigation() {
        onView(withId(R.id.UsernameField)).perform(TypeTextAction("dmytro"), ViewActions.closeSoftKeyboard())
        onView(withId(R.id.PasswordField)).perform(TypeTextAction("dmytro"), ViewActions.closeSoftKeyboard())
        onView(withId(R.id.ValidateBtn)).perform(click())
        // Can't test because api take time to send data Need to mock call api
    }

}