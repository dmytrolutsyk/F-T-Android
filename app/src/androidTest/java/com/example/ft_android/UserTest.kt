package com.example.ft_android
import PatchUserResult
import SignInResult
import SignUpBody
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith
import com.example.ft_android.User
import org.junit.Assert

@RunWith(AndroidJUnit4::class)
class UserTest {

    @Test
    fun user() {
        val user: User = User("toto", "toto", "Versailles", "toto75012@myges.fr", "particular", "+33070856374",
            "10/05/2020", "10/05/2020", "ok")
       Assert.assertEquals("toto", user.username)
    }

    @Test
    fun signInResult(){
        val signin: SignInResult = SignInResult("error", "X6jhgfsj", "fkjdshfkjdskjfhdskjhfksdfkhsdfb")
        Assert.assertEquals(32, signin.token.length)
    }

    @Test
    fun signup(){
        val signup: SignUpBody = SignUpBody("toto", "dmytro")
        Assert.assertSame(signup.password, signup.username)
    }

    @Test
    fun patchUserResult() {
        val patchUserResult: PatchUserResult = PatchUserResult("", User("toto", "toto", "Versailles", "toto75012@myges.fr", "particular", "+33070856374",
            "10/05/2020", "10/05/2020", "ok")
        )
        Assert.assertNotNull("", patchUserResult.user)
    }

    @Test
    fun mainActivity(){
        val mainActivity: MainActivity = MainActivity()
        Assert.assertNotNull("", mainActivity.loginUser)
    }

    @Test
    fun bottomNavigationActivity(){
        val bottomNavigationActivity: BottomNavigationActivity = BottomNavigationActivity()
        Assert.assertNotNull("", bottomNavigationActivity.assets )
    }

}